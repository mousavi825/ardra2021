using UnityEngine;

public class CustomColor : MonoBehaviour
{
    public void CarBodyRed()
    {
        GameObject CarBody = GameObject.Find("Body");
        CarBody.GetComponent<Renderer>().sharedMaterial.color = Color.red;

    }
    public void CarBodyBlue()
    {
        GameObject CarBody = GameObject.Find("Body");
        CarBody.GetComponent<Renderer>().sharedMaterial.color = Color.blue;

    }
    public void CarBodyYellow()
    {
        GameObject CarBody = GameObject.Find("Body");
        CarBody.GetComponent<Renderer>().sharedMaterial.color = Color.yellow;

    }
    public void CarBodyWhite()
    {
        GameObject CarBody = GameObject.Find("Body");
        CarBody.GetComponent<Renderer>().sharedMaterial.color = Color.white;

    }
    public void CarBodyBlack()
    {
        GameObject CarBody = GameObject.Find("Body");
        CarBody.GetComponent<Renderer>().sharedMaterial.color = Color.black;

    }
    public void CarBodyCyan()
    {
        GameObject CarBody = GameObject.Find("Body");
        CarBody.GetComponent<Renderer>().sharedMaterial.color = Color.cyan;

    }
}

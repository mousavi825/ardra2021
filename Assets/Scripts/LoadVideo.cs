using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LoadVideo : MonoBehaviour
{

    void Update()
    {
        if(Input.touchCount > 0 && Input.touches[0].phase == TouchPhase.Began)
        {
            Ray ray = Camera.main.ScreenPointToRay(Input.touches[0].position);
            RaycastHit hit;

            if(Physics.Raycast(ray, out hit))
            {
                if(hit.collider != null)
                {
                    Application.OpenURL("https://www.youtube.com/watch?v=s9hgIAfe-f0");
                }
            }
        }

#if UNITY_EDITOR
        if(Input.GetMouseButtonDown(0))
        {
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            RaycastHit hit;

            if (Physics.Raycast(ray, out hit))
            {
                if (hit.collider != null)
                {
                    Application.OpenURL("https://www.youtube.com/watch?v=s9hgIAfe-f0");
                }
            }
        }
#endif
    }
}

using UnityEngine;

public class BrakeDiscChange : MonoBehaviour
{

    enum states { stageOne, stageTwo, stageThree}
    private int actualState = 0;

    public void DisassembleArrowClicked()
    {
        switch (actualState)
        {
            case (int)states.stageOne:
                //call method for first assemble stage (Reverse Disassemble Stage 1)
                StageOneDisassembleObject();
                actualState++;
                break;

            case (int)states.stageTwo:
                //call method for second assemble stage (Reverse Disassemble Stage 2)
                StageTwoDisassembleObject();
                actualState++;
                break;

            case (int)states.stageThree:
                //call method for third assemble stage (Reverse Disassemble Stage 3)
                StageThreeDisassembleObject();
                actualState++;
                break;
        }
    }

        public void AssembleArrowClicked()
    {
        switch (actualState - 1)
        {
            case (int)states.stageOne:
                //call method for first assemble stage (Reverse Disassemble Stage 1)
                StageOneAssembleObject();
                actualState--;
                break;

            case (int)states.stageTwo:
                //call method for second assemble stage (Reverse Disassemble Stage 2)
                StageTwoAssembleObject();
                actualState--;
                break;

            case (int)states.stageThree:
                //call method for third assemble stage (Reverse Disassemble Stage 3)
                StageThreeAssembleObject();
                actualState--;
                break;
        }
    }


    // Assemble and Disassemble Methods
    void StageOneDisassembleObject()
    {
        GameObject CarWheel = GameObject.Find("Full-FrontWheelRight-Parent");
        CarWheel.GetComponent<Animator>().SetBool("open", true);
    }
    void StageTwoDisassembleObject()
    {
        GameObject CarBrake = GameObject.Find("Brake-Change");
        CarBrake.GetComponent<Animator>().SetBool("scheibe", true);
    }
    void StageThreeDisassembleObject()
    {
        GameObject CarDisk = GameObject.Find("Disk-Change");
        CarDisk.GetComponent<Animator>().SetBool("diskBool", true);
    }
    
    void StageOneAssembleObject()
    {
        GameObject CarWheel = GameObject.Find("Full-FrontWheelRight-Parent");
        CarWheel.GetComponent<Animator>().SetBool("open", false);
    }
    void StageTwoAssembleObject()
    {
        GameObject CarBrake = GameObject.Find("Brake-Change");
        CarBrake.GetComponent<Animator>().SetBool("scheibe", false);
    }
    void StageThreeAssembleObject()
    {
        GameObject CarDisk = GameObject.Find("Disk-Change");
        CarDisk.GetComponent<Animator>().SetBool("diskBool", false);
    }

}


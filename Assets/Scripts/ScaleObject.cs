using UnityEngine;
using UnityEngine.UI;

[ExecuteInEditMode]
public class ScaleObject : MonoBehaviour
{
    public float scale;
    public Slider slider;

    void Start()
    {
        //Cache the Slider and the ParticleSystem variables
        slider = GameObject.Find("Slider").GetComponent<Slider>();
    }

    void OnEnable()
    {
        //Subscribe to the Slider Click event
        slider.onValueChanged.AddListener(delegate { sliderCallBack(slider); });
    }

    //Will be called when Slider changes
    public void sliderCallBack(Slider slider)
    {
        scale = slider.value;
    }

    private void Update()
    {
        transform.localScale = new Vector3(scale, scale, scale);
    }

}